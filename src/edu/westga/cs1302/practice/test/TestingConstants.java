package edu.westga.cs1302.practice.test;

/**
 * The class that stores Testing Constants.
 * 
 * @author	CS1302
 * @version Fall 2021
 */
public class TestingConstants {

	public static final double DELTA = 0.000001;
}
