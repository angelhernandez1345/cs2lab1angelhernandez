package edu.westga.cs1302.practice.test.patient;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.practice.model.Patient;

/**
 * Ensure correct functionality of the Patient constructor.
 * 
 * @author	CS1302
 * @version Fall 2021
 */
public class TestConstruction {

	@Test
	public void testValidConstructionOfPatientObject() {
		LocalDate dob = LocalDate.of(1900, 8, 12);
		Patient patient = new Patient(708L, dob);
		assertAll(() -> assertEquals(708L, patient.getPatientID()), () -> assertEquals(dob, patient.getDateOfBirth()));
	}

	@Test
	public void testNullDob() {
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new Patient(708L, null));
		assertEquals(Patient.DATE_NULL, exception.getMessage());
	}

	@Test
	public void testInvalidIdJustBelowThreshold() {
		LocalDate dob = LocalDate.of(2000, 8, 12);
		long idBelowThreshold = -1L;
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new Patient(idBelowThreshold, dob));
		assertEquals(Patient.ID_NEGATIVE, exception.getMessage());
	}

	@Test
	public void testValidIdExactlyAtThreshold() {
		LocalDate dob = LocalDate.of(2000, 8, 12);
		long idThreshold = 0L;
		Patient patient = new Patient(idThreshold, dob);
		assertAll(() -> assertEquals(idThreshold, patient.getPatientID()),
				() -> assertEquals(dob, patient.getDateOfBirth()));

	}

	@Test
	public void testValidIdJustAboveThreshold() {
		LocalDate dob = LocalDate.of(2000, 8, 12);
		long idAboveThreshold = 1L;
		Patient patient = new Patient(idAboveThreshold, dob);
		assertAll(() -> assertEquals(idAboveThreshold, patient.getPatientID()),
				() -> assertEquals(dob, patient.getDateOfBirth()));
	}

	@Test
	public void testInvalidDobJustBelowThredhold() {
		LocalDate dobBelowThredhold = Patient.MIN_DATE.minus(1, ChronoUnit.DAYS);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new Patient(708L, dobBelowThredhold));
		assertEquals(Patient.DATE_INVALID, exception.getMessage());
	}

	@Test
	void testValidDobExactyAtThreshold() {
		LocalDate dobThreshold = Patient.MIN_DATE;
		long id = 917L;
		Patient patient = new Patient(id, dobThreshold);
		assertAll(() -> assertEquals(id, patient.getPatientID()),
				() -> assertEquals(dobThreshold, patient.getDateOfBirth()));
	}

	@Test
	public void testValidDobJustAboveThredhold() {
		LocalDate dobAboveThreshold = Patient.MIN_DATE.plus(1, ChronoUnit.DAYS);
		long id = 917L;
		Patient patient = new Patient(id, dobAboveThreshold);
		assertAll(() -> assertEquals(id, patient.getPatientID()),
				() -> assertEquals(dobAboveThreshold, patient.getDateOfBirth()));
	}
}
