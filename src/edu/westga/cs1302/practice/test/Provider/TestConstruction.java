package edu.westga.cs1302.practice.test.Provider;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.practice.model.Provider;

/**
 * Ensure correct functionality of the Provider constructor.
 * 
 * @author	Angel Hernandez
 * @version 08/19/2021
 */
public class TestConstruction {

	@Test
	public void testValidConstructionOfProviderObject() {
		Provider provider = new Provider("West View");
		assertEquals("West View", provider.getHealthCareProviderName());
	}

	@Test
	public void testDefaultConstructor() {
		Provider provider = new Provider();
		assertEquals("", provider.getHealthCareProviderName());
	}
	
	@Test
	public void testNullName() {
		assertThrows(IllegalArgumentException.class,
					 () -> new Provider(null));
	}
	
	@Test
	public void testEmptyName() {
		assertThrows(IllegalArgumentException.class,
					 () -> new Provider(""));
	}
}
