package edu.westga.cs1302.practice.model;

import java.time.LocalDate;

/**
 * This class represents a Patient.
 * 
 * @author	Angel Hernandez
 * @version 08/17/2021
 */
public class Patient {

	public static final String ID_NEGATIVE = "ID cannot be negative.";
	public static final String DATE_INVALID = "DOB cannot be before 1900.";
	public static final String DATE_NULL = "DOB cannot be null.";
	public static final LocalDate MIN_DATE = LocalDate.of(1900, 1, 1);
	private long patientID;
	private LocalDate dateOfBirth;

	/**
	 * Instantiates a new patient object.
	 * 
	 * @precondition patientID >= 0 && dateOfBirth != null && dateOfBirth >=
	 *               MIN_DATE
	 * @postcondition getPatientID() == patientID && getDateOfBirth() == dateOfBirth
	 * @param patientID   the ID of this patient
	 * @param dateOfBirth the date of birth of this patient
	 */
	public Patient(long patientID, LocalDate dateOfBirth) {
		if (patientID < 0) {
			throw new IllegalArgumentException(ID_NEGATIVE);
		}
		if (dateOfBirth == null) {
			throw new IllegalArgumentException(DATE_NULL);
		}
		if (dateOfBirth.isBefore(MIN_DATE)) {
			throw new IllegalArgumentException(DATE_INVALID);
		}
		this.patientID = patientID;
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * Returns the patientID of this patient.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the patientID
	 */
	public long getPatientID() {
		return this.patientID;
	}

	/**
	 * Sets the patientID of this patient.
	 * 
	 * @precondition patientID >= 0
	 * @postcondition getPatientID() == patientID
	 * @param patientID the patientID to set
	 */
	public void setPatientID(long patientID) {
		if (patientID < 0) {
			throw new IllegalArgumentException(ID_NEGATIVE);
		}
		this.patientID = patientID;
	}

	/**
	 * Returns the date of birth of this patient.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the dateOfBirth of this patient
	 */
	public LocalDate getDateOfBirth() {
		return this.dateOfBirth;
	}

	/**
	 * Sets the date of birth of this patient
	 * 
	 * @precondition dateOfBirth != null && dateOfBirth >= MIN_DATE
	 * @postcondition getDateOfBirth() == dateOfBirth
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(LocalDate dateOfBirth) {
		if (dateOfBirth == null) {
			throw new IllegalArgumentException(DATE_NULL);
		}
		if (dateOfBirth.isBefore(MIN_DATE)) {
			throw new IllegalArgumentException(DATE_INVALID);
		}
		this.dateOfBirth = dateOfBirth;
	}
}