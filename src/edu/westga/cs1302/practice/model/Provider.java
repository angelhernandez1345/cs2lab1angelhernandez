package edu.westga.cs1302.practice.model;

import java.util.ArrayList;

/**
 * This class creates a Provider.
 * 
 * @author 	Angel Hernandez
 * @version 08/18/2021
 */
public class Provider {

	private String healthCareProviderName;
	private ArrayList<Patient> patients;
	
	/**
	 * Creates a new Provider object and Initializes all data members.
	 * 
	 * @precondition none
	 * @postcondition getHealthCareProviderName() == "" &&
	 * 				  getPatients() == 0
	 */
	public Provider() {
		this.healthCareProviderName = "";
		this.patients = new ArrayList<Patient>();	
	}
	
	/**
	 * Instantiates a new Provider object.
	 * 
	 * @precondition name != null && !name.isEmpty()
	 * @postcondition getHealthCareProviderName() == name &&
	 * 				  getPatients() == 0
	 * 
	 * @param name the name of the health care provider
	 */
	public Provider(String name) {
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException("A valid name must be entered");
		}
		this.healthCareProviderName = name;
		this.patients = new ArrayList<Patient>();	
	}

	/**
	 * Returns the healthCareProviderName of this Provider.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the healthCareProviderName
	 */
	public String getHealthCareProviderName() {
		return this.healthCareProviderName;
	}

	/**
	 * Sets the healthCareProviderName of this Provider.
	 *
	 * @precondition healthCareProviderName != null && !healthCareProviderName.isEmpty()
	 * @postcondition getHealthCareProviderName == healthCareProviderName
	 * 
	 * @param healthCareProviderName the healthCareProviderName to set
	 */
	public void setHealthCareProviderName(String healthCareProviderName) {
		if (healthCareProviderName == null || healthCareProviderName.isEmpty()) {
			throw new IllegalArgumentException("A valid healthCareProviderName must be entered");
		}
		this.healthCareProviderName = healthCareProviderName;
	}

	/**
	 * Returns the collection of all patients of this Provider.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the collection of all patients
	 */
	public ArrayList<Patient> getPatients() {
		return this.patients;
	}
	
	/**
	 * Returns the number of patients served by the provider.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number of patients served by the provider
	 */
	public int numberOfPatients() {
		return this.patients.size();
	}
}
